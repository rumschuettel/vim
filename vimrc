call plug#begin()
" gui and editor
Plug 'rumschuettel/vim-nellys-star'
Plug 'tpope/vim-sensible'
Plug 'vim-airline/vim-airline'
Plug 'sjl/gundo.vim', { 'on': 'GundoToggle' }
Plug 'scrooloose/nerdtree'

" programming
Plug 'w0rp/ale'
"Plug 'rust-lang/rust.vim', { 'for': 'rust' }
"Plug 'Nonius/cargo.vim', { 'for': 'rust' }
Plug 'fsharp/vim-fsharp', { 'for': 'fsharp', 'do': 'make fsautocomplete' }
Plug 'Valloric/YouCompleteMe'
"Plug 'ambv/black'

" writing
Plug 'junegunn/goyo.vim'
call plug#end()



" GUI

" fonts for gui
if has('gui_running')
  if has('win32')
    set guifont=Powerline_Consolas:h11
  else
    set guifont=Droid\ Sans\ Mono\ for\ Powerline\ 10,Ubuntu\ Mono\ derivative\ Powerline\ 11
  endif
else
  set t_Co=256
endif

" display line numbers
set number

" color scheme and some extras for the cursor
set background=dark
colorscheme nellys-star 

" airline special symbols
let g:airline_powerline_fonts = 1

" airline buffer list
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#left_sep = ' '
let g:airline#extensions#tabline#left_alt_sep = ''

" underline current line
set cursorline

" gvim no toolbar or menu
set go-=m go-=T go-=l go-=L go-=r go-=R go-=b go-=F
set fcs+=vert:│

" mouse support
set mouse=a

" solves utf8-bug for old terminals in nvim; remove at some point
set guicursor=


" EDITOR

" new assignment for leader key
let mapleader = ','

" cycle through buffers without saving
set hidden

" move among buffers with CTRL
map <C-J> :bnext<CR>
map <C-K> :bprev<CR>

" close buffers with leader
map ,x :bp<CR> :bd #<CR>

" undo tree
nnoremap <F5> :GundoToggle<CR>

" NERDTree automatically start when vim starts without commands, and close if
" only window
nnoremap <F6> :NERDTreeToggle<CR>
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif

" wildcard menu
set wildmenu
set wildmode=list:longest,full

" indentation: 4 spaces, tabs are not used
set tabstop=4 softtabstop=4 shiftwidth=4 expandtab

" undo history
set history=10000

" basic keybindings like in windows
source $VIMRUNTIME/mswin.vim
behave mswin



" FILETYPE SPECIFIC

let g:ale_sign_error = 'E '
let g:ale_sign_warning = 'I ' 

let g:airline#extensions#ale#enabled = 1
let g:ale_lint_on_text_changed = 0
let g:ale_lint_on_save = 1

let g:ale_linters = {
            \'cpp': ['clangtidy']
            \} 

" cpp
let g:ale_cpp_clangtidy_checks = ['*','-misc-unused-parameters','-llvm-header-guard','-*-braces-around-statements','-*array-to-pointer-decay']
let g:ale_cpp_clangtidy_builddir = '.'

" rust
hi! def link rustModPath None
hi! def link rustModPathSep None
hi! def link rustMacroVariable Macro
hi! def link rustDeriveTrait None
hi! def link rustTrait None
hi! def link rustIdentifier None
hi! def link rustSelf Keyword
hi! def link rustRepeat Keyword
hi! def link rustFuncCall None


" CUSTOM COMMANDS

" move among errors
map ,k :lnext<CR>
map ,l :lprev<CR>
"nmap <silent> <C-k> <Plug>(ale_previous_wrap)
"nmap <silent> <C-j> <Plug>(ale_next_wrap)

" delete trailing whitespaces
command! RemoveTrailing %s/\s\+$//e

" get highlight group
map <F10> :echo "hi<" . synIDattr(synID(line("."),col("."),1),"name") . '> trans<'
            \ . synIDattr(synID(line("."),col("."),0),"name") . "> lo<"
            \ . synIDattr(synIDtrans(synID(line("."),col("."),1)),"name") . ">"<CR>
